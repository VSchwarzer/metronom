package passwordgenerator

import (
	"crypto/rand"
	"errors"
	"math/big"
	"strings"
)

type PasswordRequirements struct {
	Numbers      int
	SpecialChars int
	Length       int
}

// Pick Random Element from String
func getRandomElement(elements string) (string, error) {
	elementIndex, error := rand.Int(rand.Reader, big.NewInt(int64(len(elements))))
	if error != nil {
		return "", error
	}
	return string(elements[elementIndex.Int64()]), nil

}

//Depending on generated randomBool vowel gets replaced by number
func randomReplacement(element string) (string, error) {

	randomBool, err := rand.Int(rand.Reader, big.NewInt(int64(1)))
	if err != nil {
		return "", err
	}

	if randomBool.Int64() == 1 {
		return vowelToNumber[strings.ToLower(element)], nil

	}

	return element, nil
}

// randomInsert concats new element randomly into partialGeneratedPassword string
func randomInsert(element, partialGeneratedPassword string) (string, error) {

	if partialGeneratedPassword == "" {
		return element, nil
	}

	randomPlacement, err := rand.Int(rand.Reader, big.NewInt(int64(len(partialGeneratedPassword))))
	if err != nil {
		return "", err
	}

	index := randomPlacement.Int64()

	return partialGeneratedPassword[0:index] + element + partialGeneratedPassword[index:len(partialGeneratedPassword)], nil

}

// iterateOverType adds n elements of type alphabet/specialChar/numbers to password
func iterateOverType(lengthPassword, lengthSetType int, charset, partialGeneratedPassword string) (string, error) {

	if lengthSetType > lengthPassword {
		return "", errors.New("Options cant be longer than whole password")
	}

	for i := 0; i < lengthSetType; i++ {

		element, err := getRandomElement(charset)

		if err != nil {
			return "", err
		}

		// Check if letter / vowel for calling randomly switch vowels to numbers (l33t conversation)
		if _, ok := vowelToNumber[strings.ToLower(element)]; ok {
			randomReplacement(element)
		}

		partialGeneratedPassword, err = randomInsert(element, partialGeneratedPassword)

		if err != nil {
			return "", err
		}
	}

	return partialGeneratedPassword, nil
}

//GeneratePassword generates random passwords based on the arguments passed to the function
func GeneratePassword(passwordRequirements PasswordRequirements) (string, error) {

	var password string

	numberLetter := passwordRequirements.Length - passwordRequirements.Numbers - passwordRequirements.SpecialChars

	// Generate partial password string with random numbers
	password, err := iterateOverType(passwordRequirements.Length, passwordRequirements.Numbers, numbers, password)

	if err != nil {
		return "", err
	}

	// Generate partial password string with random special characters
	password, err = iterateOverType(passwordRequirements.Length, passwordRequirements.SpecialChars, specialChar, password)

	if err != nil {
		return "", err
	}

	// Generate partial password string with random letters
	password, err = iterateOverType(passwordRequirements.Length, numberLetter, alphabet, password)

	if err != nil {
		return "", err
	}

	return password, nil

}
