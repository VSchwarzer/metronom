package passwordgenerator

//Symbols allowed / used in password generation
const (
	alphabet    = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	numbers     = "0123456789"
	specialChar = "!#$%&'()*+,-./:;<=>?@^_`{|}~"
)

/* There is no common number equivalent for the vowel u so I picked randomly the number 6 */

var vowelToNumber = map[string]string{
	"a": "4",
	"e": "3",
	"i": "1",
	"o": "0",
	"u": "6",
}
