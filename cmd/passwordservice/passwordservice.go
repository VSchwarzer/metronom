package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"internal/passwordgenerator"

	"github.com/gorilla/mux"
)

func main() {

	var port = flag.String("port", "8080", "Configures port the server listens")

	flag.Parse()

	fmt.Println("Password Generation Server was started")
	fmt.Printf("Server running at: 127.0.0.1:%s\n", *port)
	fmt.Printf("Generate random passwords by running 'curl 127.0.0.1:%s/{length}/{specialchars}/{numbers}/{numberofpasswords}'\n", *port)

	r := mux.NewRouter()
	r.HandleFunc("/{length:[0-9]+}/{specialchars:[0-9]+}/{numbers:[0-9]+}", generatePassword).Methods("GET")
	r.HandleFunc("/{length:[0-9]+}/{specialchars:[0-9]+}/{numbers:[0-9]+}/{numberofpasswords:[0-9]+}", generatePassword).Methods("GET")

	//Start Local Server on defined
	log.Fatal(http.ListenAndServe(":"+*port, r))

}

// GeneratePassword calls the ... package to generate n random passwords
// The properties of the generated password(s) are depending on the variables:
//	- length : Length of randomly generated Password
//	- specialchars: Occurence of Special Characters in Password
//	- numbers: Occurence of Numbers in Password
func generatePassword(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	//Convert passed variables to Integer
	//We can ignore err since we prevented wrong number formats using a regex
	length, _ := strconv.Atoi(vars["length"])
	specialchars, _ := strconv.Atoi(vars["specialchars"])
	numbers, _ := strconv.Atoi(vars["numbers"])

	// Default number of paswords
	numberOfPasswords := 1

	//Check if number of passwords is set
	if vars["numberofpasswords"] != "" {
		var err error
		numberOfPasswords, err = strconv.Atoi(vars["numberofpasswords"])

		if err != nil {
			log.Printf("error: %s", err)
		}
	}

	//Slice with generated passwords
	var passwords []string

	passwordRequirements := passwordgenerator.PasswordRequirements{Length: length,
		Numbers:      numbers,
		SpecialChars: specialchars}

	for i := 0; i < numberOfPasswords; i++ {
		password, err := passwordgenerator.GeneratePassword(passwordRequirements)
		if err != nil {
			log.Printf("error: %s", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}
		passwords = append(passwords, password)

	}

	w.Header().Set("Content-Type", "application/json")

	payload, err := json.Marshal(passwords)
	if err != nil {
		log.Printf("error: %s", err)
		http.Error(w, "Internal JSON Parsing failed", http.StatusInternalServerError)
	}
	w.Write(payload)
}
