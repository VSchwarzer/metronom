GO_SRCS = $(shell find cmd -name '*.go')
GO_PATH := $(shell go env GOPATH)
GO_BIN := $(GO_PATH)/bin


all: install

install:
	go build ${GO_SRCS}